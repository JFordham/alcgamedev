using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{

    // References for door objects
    [Header("Door Objects")]
    public Transform northDoor;
    public Transform southDoor;
    public Transform westDoor;
    public Transform eastDoor;
    
    // References for wall objects
    [Header("Wall Objects")]
    public Transform northWall;
    public Transform southWall;
    public Transform westWall;
    public Transform eastWall;
    
    // How many tiles are there in the room
    [Header("Room Size")]
    public int insideWidth;
    public int insideHeight;
    public float tileSize = 0.16f;

    // Objects to instantiate
    [Header("Room Prefabs")]
    public GameObject enemyPrefab;
    public GameObject coinPrefab;
    public GameObject healthPrefab;
    public GameObject keyPrefab;
    public GameObject exitDoorPrefab;

    // List of positions to aviod layering objects while instantiating
    private List<Vector3> usedPositions = new List<Vector3>();

    public void GenerateInterior() 
    {
        // Generate enemies
        if(Random.value < Generation.instance.enemySpawnChance) {
            SpawnPrefab(enemyPrefab, 1, Generation.instance.maxEnemiesPerRoom + 1);
        }
        // Generate Coins
        if(Random.value < Generation.instance.coinSpawnChance) {
            SpawnPrefab(coinPrefab, 1, Generation.instance.maxCoinsPerRooom + 1);
        }
        // Generate Health
        if(Random.value < Generation.instance.healthSpawnChance) {
            SpawnPrefab(healthPrefab, 1, Generation.instance.maxHealthPerRoom + 1);
        }
    }

    public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0) 
    {
        int num = 1;
        if (min != 0 || max > min) {
            num = Random.Range(min,max);
        }
        // Spawn Prefabs
        for (int i = 0; i < num; i++) {
            // Instantiate Prefabs
            GameObject obj = Instantiate(prefab);
            //Get the nearest tile to a random position inside the room
            Vector3 pos = transform.position + new Vector3(Random.Range(-insideWidth/2, insideWidth/2)*tileSize, Random.Range(-insideHeight/2, insideHeight/2)*tileSize,0);
            //Place prefab to random position just Generated
            obj.transform.position = pos;
            usedPositions.Add(pos);

            if (prefab == enemyPrefab) {
                EnemyManager.instance.enemies.Add(obj.GetComponent<Enemy>());
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
