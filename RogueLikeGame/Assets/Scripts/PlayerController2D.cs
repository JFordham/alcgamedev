using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController2D : MonoBehaviour
{
    public int HP;
    public int maxHP;
    public int coin;

    public bool hasKey;
    
    public SpriteRenderer sr;

    [Header("Movement")]

    public float speed;

    // Rigidbody2D rb;

    //Layer to avoid (Mask)
    public LayerMask moveLayerMask;

    [Header("Attack and Damage")]

    public float flashDelay = 0.5f;

    public float attackDistance = 0.16f;
    public int attackDamage = 1;
    public float tileSize = 0.16f;

    // void Start()
    // {
    //     rb = GetComponent<Rigidbody2D>();
    // }

    void Move(Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);

        // rb.velocity = new Vector2(dir.x * speed, dir.y * speed);

        if (hit.collider == null)
        {
            // rb.velocity = new Vector2(dir.x * speed, dir.y * speed);
            transform.position += new Vector3(dir.x * tileSize, dir.y * tileSize);
            // Move the Enemies
            EnemyManager.instance.OnPlayerMove();
        }

        // Recenter on tile
        float xError = 0.0f;
        float yError = 0.0f;
        int i = 0;
        while (xError > transform.position.x + tileSize/2 || xError < transform.position.x - tileSize/2) {
            if (xError < transform.position.x) {
                xError = tileSize * i;
            } else {
                xError = tileSize * i * -1;
            }
            i++;
        }
        i = 0;
        while (yError > transform.position.y + tileSize/2 || yError < transform.position.y - tileSize/2) {
            if (yError < transform.position.y) {
                yError = tileSize * i;
            } else {
                yError = tileSize * i * -1;
            }
            i++;
        }

    }

    public void OnMoveUp(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            Move(Vector2.up);
        }
    }
    public void OnMoveDown(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            Move(Vector2.down);
        }
    }
    public void OnMoveLeft(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            Move(Vector2.left);
        }
    }
    public void OnMoveRight(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            Move(Vector2.right);
        }
    }

    public void OnAttackUp(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            TryAttack(Vector2.up);
        }
    }
    public void OnAttackDown(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            TryAttack(Vector2.down);
        }
    }
    public void OnAttackLeft(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            TryAttack(Vector2.left);
        }
    }
    public void OnAttackRight(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) {
            TryAttack(Vector2.right);
        }
    }

    public void TakeDamage(int dmg) 
    {
        HP -= dmg;
        UI.instance.UpdateHealth(HP);
        StartCoroutine(DamageFlash());

        if (HP <= 0) {
            SceneManager.LoadScene(0);
        }
    }

    IEnumerator DamageFlash()
    {
        // Get a reference to the default sprite color
        Color defaultColor = sr.color;
        // Set the color to white
        sr.color = Color.white;
        // Wait for a period of time before changing color
        yield return new WaitForSeconds(flashDelay);
        // Reset the Color to original
        sr.color = defaultColor;
    }

    void TryAttack (Vector2 dir)
    {
        // Raycast Hit that ignores from layer 1 to 7
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, attackDistance, 1<<7);

        if (hit.collider != null) {
            hit.transform.GetComponent<Enemy>().TakeDamage(attackDamage);
        }
    }

    public void AddCoin(int amount) 
    {
        coin += amount;
        UI.instance.UpdateCoinText(coin);
    }

    public bool AddHealth(int amount)
    {
        if (HP + amount <= maxHP) {
            HP += amount;
            UI.instance.UpdateHealth(HP);
            return true;
        } else if (HP < maxHP) {
            HP = maxHP;
            UI.instance.UpdateHealth(HP);
            return true; 
        }
        return false;
    }

    /*
    private float hInput;
    private float vInput;
    public float speed;
    public float turnSpeed;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");
        
        rb.velocity = new Vector2(hInput * speed, vInput * speed);
    }
    */
}
