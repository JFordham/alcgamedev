using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Attributes")]
    public int health;
    public int damage;

    [Header("References")]
    public GameObject deathDropPrefab;
    public SpriteRenderer sr;
    public PlayerController2D player;

    public LayerMask moveLayerMask;
    public float flashDelay = 0.05f;
    public float attackChance  = 0.5f;
    public int checkLimit = 8;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move() 
    {
        if (Random.value < 0.5f) {
            return;
        }
        Vector3 dir = Vector3.zero;
        bool canMove = false;

        // Before moving the enemy, get a random direction to move to.
        int checkIter = 0;
        while (!canMove) {
            dir = GetRandomDirection();
            // Cast a ray into the direction
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);

            // If the ray hasn't detected any obstacles
            if (hit.collider == null) {
                canMove = true;
            }
            checkIter++;
            if (!canMove && checkIter >= checkLimit) {
                break;
            }
        }
        if (canMove) {
            // Move towards the direction
            transform.position += dir * 0.16f;
        }
    }

    Vector3 GetRandomDirection() 
    {
        // Get a random number between 0 and 4
        int rand = Random.Range(0,4);
        switch (rand) {
            case 0: return Vector3.up;
            case 1: return Vector3.down;
            case 2: return Vector3.left;
            case 3: return Vector3.right;
        }
        return Vector3.zero;
    }

    public void TakeDamage(int dmg) 
    {
        health -= dmg;

        if (health <= 0) {
            if (deathDropPrefab != null) {
                Instantiate(deathDropPrefab, transform.position, transform.rotation);
            }
            Destroy(gameObject);
        }

        StartCoroutine(DamageFlash());

        if (Random.value > attackChance) {
            player.TakeDamage(damage); 
        }
    }

    IEnumerator DamageFlash()
    {
        // Get a reference to the default sprite color
        Color defaultColor = sr.color;
        // Set the color to white
        sr.color = Color.red;
        // Wait for a period of time before changing color
        yield return new WaitForSeconds(flashDelay);
        // Reset the Color to original
        sr.color = defaultColor;
    }
}
