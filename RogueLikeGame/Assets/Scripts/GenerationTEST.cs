using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GenerationTEST : MonoBehaviour
{
    [Header("Map Properties")]
    public int mapWidth = 7;
    public int mapHeight = 7;
    public float roomSize = 12f;
    public float tileSize = 0.16f;
    public int roomsToGenerate = 12;
    public bool FillMap = true;

    private int roomCount;
    private bool roomsInstantiated;

    // Store the origin of the first room to generate procedural dungeon. 
    private Vector2 firstRoomPos;

    // A 2D boolean array to map out the level
    private bool[,] map;
    // The room prefab to instantiate
    public GameObject roomPrefab;

    private List<Room> roomObjects = new List<Room>();

    //Creating a Singleton
    public static GenerationTEST instance; 

    // Generation Seed Variable
    public int seed = 123123;

    void Awake()
    {
        instance = this; 
    }

    // Start is called before the first frame update
    void Start()
    {
        // Random Seed Assigned to a random number generator
        Random.InitState(seed);
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Generate()
    {
        // Create a new map of the specified size
        map = new bool[mapWidth, mapHeight];
        // Check to see if we can place a room in the center of the map.
        // CheckRoom(mapWidth/2, mapHeight/2, 0, Vector2.zero, true);
        InstantiateRooms();
        //Find the player in the scene, and position them inside the first room.
        FindObjectOfType<PlayerController2D>().transform.position = firstRoomPos * roomSize * tileSize; 
        Debug.Log($"Generated Rooms: {roomCount}/{roomsToGenerate}");
    }


    void CheckRoom(int x, int y, int remaining, bool firstRoom = false)
    {
        // If we have generated all of the rooms that we need, stop checking the rooms.
        if(roomCount >= roomsToGenerate) {
            return;
        }
        // If this is outside the bounds of the actual  map, stop the function.
        if(x < 0 || x > mapWidth - 1 || y < 0 || y > mapHeight - 1) {
            return;
        }
        // If this is not the first room, and there is no more room to check, stop the function.
        if(firstRoom == false && remaining <= 0) {
            return;
        }
        // If the given map tile is already occupied, stop the function.
        if(map[x,y] == true)
            return;
        // If this is the first room, store the room position.
        if(firstRoom == true)
            firstRoomPos = new Vector2(x,y);
        
        // Add one to roomCount and set the map tile to be true.
        roomCount++;
        map[x,y] = true;
    }


    void InstantiateRooms()
    {
        if (roomsInstantiated) {
            return;
        }

        roomsInstantiated = true;

        for (int x=0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                if (map[x,y] == false) {
                    continue;
                }
                // Instantiate a new room prefab
                GameObject roomObj = Instantiate(roomPrefab, new Vector3(x,y,0)*roomSize*tileSize, Quaternion.identity);
                // Get reference to the room script of the new room object
                Room room = roomObj.GetComponent<Room>();

                // If we're within the boundary of the map, and if there is room above us
                if (y<mapHeight-1 && map[x,y+1] == true) {
                    // Enable the north door and disable the north wall
                    room.northDoor.gameObject.SetActive(true);
                    room.northWall.gameObject.SetActive(false);
                } else {
                    // Disable the north door and disable the north wall
                    room.northDoor.gameObject.SetActive(false);
                    room.northWall.gameObject.SetActive(true);
                }

                if (y>0 && map[x,y-1] == true) {
                    // Enable the south door and disable the south wall
                    room.southDoor.gameObject.SetActive(true);
                    room.southWall.gameObject.SetActive(false);
                } else {
                    // Disable the north door and disable the north wall
                    room.southDoor.gameObject.SetActive(false);
                    room.southWall.gameObject.SetActive(true);
                }

                if (x<mapWidth-1 && map[x+1,y] == true) {
                    // Enable the east door and disable the east wall
                    room.eastDoor.gameObject.SetActive(true);
                    room.eastWall.gameObject.SetActive(false);
                } else {
                    // Disable the north door and disable the north wall
                    room.eastDoor.gameObject.SetActive(false);
                    room.eastWall.gameObject.SetActive(true);
                }

                if (x>0 && map[x-1,y] == true) {
                    // Enable the west door and disable the west wall
                    room.westDoor.gameObject.SetActive(true);
                    room.westWall.gameObject.SetActive(false);
                } else {
                    // Disable the north door and disable the north wall
                    room.westDoor.gameObject.SetActive(false);
                    room.westWall.gameObject.SetActive(true);
                }

                // If this is not the first room, call GenerateInterior();
                if (firstRoomPos != new Vector2(x,y)) {
                    room.GenerateInterior();
                }

                // Add the room to the roomObjects list
                roomObjects.Add(room);
            }
        }
        // After looping through every element inside the map array, call CalculateKeyandExit();
        CalculateKeyandExit();
    }

    void CalculateKeyandExit()
    {
        
    }
}
